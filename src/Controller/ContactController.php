<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Contact;
use App\Form\Type\ContactFormType;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\Bridge\Google\Transport\GmailSmtpTransport;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class ContactController extends AbstractController
{
    public function new(Request $request, MailerInterface $mailer)
    {

       
        $contact = new Contact();
        $contact->setName('Felix');
        $contact->setEmail('tettehfelix770@yahoo.com');
        $contact->setQuery('I require your services');

        $form = $this->createForm(ContactFormType::class, $contact);
        
        
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // get contact data to do stuff
            $contact = $form->getData();
           
            //persist to the database

            //please uncomment next three lines below once your database is connected 
            // $entityManager = $this->getDoctrine()->getManager();
            // $entityManager->persist($contact);
            // $entityManager->flush();
            


            // send email
            // Mailer Dsn used in env file.
            // MAILER_DSN=smtp://81b8652859bb9f:fd5e0a188fe5c6@smtp.mailtrap.io:2525

            //if(dataIsSavedtoDatabaseSuccessfullyFunc)
            //{
            $email = (new Email())
            ->from('tettehfelix770@yahoo.com')
            ->to($contact->getEmail())
            ->subject($contact->getQuery())
            ->text('Thanks for contacting us')
            ->html('<p>We will get in touch</p>');

            $mailer->send($email);
            //}
            return $this->redirectToRoute('app_contact_result_page');
        }
        return $this->render('contact/feedbackform.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    public function result(){
        return $this->render('contact/feedbackformsuccess.html.twig');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => contact::class
            // 'require_name' => false,
        ]);
        // $resolver->setAllowedTypes('require_name', 'bool');
    }
}